package com.bryantoapanta.paises.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bryantoapanta.paises.R;
import com.bryantoapanta.paises.entity.Pais;
import com.bumptech.glide.Glide;

import java.util.List;

public class AdapterPais extends RecyclerView.Adapter<AdapterPais.ViewHolderCountry>
    implements View.OnClickListener {

    private List<Pais> paisList;
    private View.OnClickListener listener;

    public AdapterPais(List<Pais> paisList) {
        this.paisList = paisList;
    }

    @Override
    public ViewHolderCountry onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view =  LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_pais, null, false);

        //evento de click
        view.setOnClickListener(this);
        return new ViewHolderCountry(view);
    }

    @Override
    public void onBindViewHolder(ViewHolderCountry viewHolderCountry, int position) {
        Pais pais = paisList.get(position);
        viewHolderCountry.nombre_pais.setText(pais.getNombre());
        viewHolderCountry.poblacion_pais.setText("Poblacion: " + pais.getPoblacion());
        viewHolderCountry.capital_pais.setText("Capital: " + pais.getCapital());
        Glide.with(viewHolderCountry.itemView.getContext()).load(pais.getUrlImagen()).into(viewHolderCountry.imageView);
    }

    @Override
    public int getItemCount() {
        return paisList.size();
    }

    //evento click
    public void setOnClickListener(View.OnClickListener listener){
        this.listener = listener;
    }

    //evento click
    @Override
    public void onClick(View v) {
        if (listener!=null){
            listener.onClick(v);
        }
    }

    public class ViewHolderCountry extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView nombre_pais;
        TextView poblacion_pais;
        TextView capital_pais;

        public ViewHolderCountry(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.id_image);
            nombre_pais = itemView.findViewById(R.id.txt_nombre_pais);
            poblacion_pais = itemView.findViewById(R.id.txt_poblacion);
            capital_pais = itemView.findViewById(R.id.txt_capital);
        }


    }
}

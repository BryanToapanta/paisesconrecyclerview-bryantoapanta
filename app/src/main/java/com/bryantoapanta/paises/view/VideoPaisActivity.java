package com.bryantoapanta.paises.view;


import android.content.Intent;
import android.os.Bundle;

import android.widget.Toast;

import com.bryantoapanta.paises.R;
import com.bryantoapanta.paises.entity.Pais;
import com.bryantoapanta.paises.util.UtilYouTubeApi;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.google.android.youtube.player.YouTubePlayerView;

public class VideoPaisActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {


    public static final String DEVELOPER_KEY = UtilYouTubeApi.DEVELOPER_KEY;
    private static String VIDEO_ID = UtilYouTubeApi.YOUTUBE_VIDEO_CODE;
    private static final int RECOVERY_DIALOG_REQUEST = 1;
    YouTubePlayerFragment myYouTubePlayerFragment;
    private String videoId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_pais);

        myYouTubePlayerFragment = (YouTubePlayerFragment) getFragmentManager().findFragmentById(R.id.videoview);


        Pais pais = (Pais) getIntent().getExtras().getSerializable("Objeto");
        setVideoId(pais.getUrlVideo());
        myYouTubePlayerFragment.initialize(UtilYouTubeApi.DEVELOPER_KEY,  VideoPaisActivity.this);
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        if (!b) {
            youTubePlayer.cueVideo(getVideoId());
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        if (youTubeInitializationResult.isUserRecoverableError()) {
            youTubeInitializationResult.getErrorDialog(this, RECOVERY_DIALOG_REQUEST).show();
        } else {
            String errorMessage = String.format(
                    "There was an error initializing the YouTubePlayer (%1$s)",
                    youTubeInitializationResult.toString());
            Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECOVERY_DIALOG_REQUEST) {
// Retry initialization if user performed a recovery action
            getYouTubePlayerProvider().initialize(DEVELOPER_KEY, VideoPaisActivity.this);
        }
    }

    protected YouTubePlayer.Provider getYouTubePlayerProvider() {
        return (YouTubePlayerView) findViewById(R.id.videoview);
    }
}
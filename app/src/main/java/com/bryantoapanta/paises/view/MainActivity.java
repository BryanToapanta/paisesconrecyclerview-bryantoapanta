package com.bryantoapanta.paises.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.bryantoapanta.paises.R;
import com.bryantoapanta.paises.adapter.AdapterPais;
import com.bryantoapanta.paises.databinding.ActivityMainBinding;
import com.bryantoapanta.paises.entity.Pais;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding activityMainBinding;
    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityMainBinding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = activityMainBinding.getRoot();
        setContentView(view);

        db = FirebaseFirestore.getInstance();

        activityMainBinding.recyclerPaises.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        activityMainBinding.recyclerPaises.setLayoutManager(layoutManager);

        consultarPaises();
    }

    public List<Pais> cargarPaises() {
        List<Pais> paises = new ArrayList<>();
        String pais;
        try {
            InputStream is = this.getResources().openRawResource(R.raw.paises);
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            if (is != null){
                while ((pais=reader.readLine()) != null){
                    paises.add(new Pais(Integer.parseInt(pais.split(";")[0])
                            ,pais.split(";")[2]
                            ,pais.split(";")[1]
                            ,pais.split(";")[3]
                            ,pais.split(";")[4]
                            ,pais.split(";")[5]));
                }
            }
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return paises;
    }

    private void consultarPaises() {
        db.collection("Pais")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            List<Pais> paises = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Map<String,Object> md = document.getData();
                                int id = ((Long) md.get("id")).intValue();
                                String urlImagen = (String) md.get("urlImagen");
                                String nombre = (String) md.get("nombre");
                                String urlVideo = (String)md.get("urlVideo");
                                String poblacion = (String)md.get("poblacion");
                                String capital = (String) md.get("capital");
                                paises.add(new Pais(id, urlImagen, nombre, urlVideo, poblacion, capital));
                            }

                            cargarRecyclerView(paises);
                        }
                    }
                });
    }

    private void cargarRecyclerView(final List<Pais> countries) {
        activityMainBinding.recyclerPaises.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        activityMainBinding.recyclerPaises.setLayoutManager(layoutManager);

        AdapterPais mAdapter = new AdapterPais(countries);
        mAdapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Pais pais = countries.get(activityMainBinding.recyclerPaises.getChildAdapterPosition(v));

                for (Pais p : cargarPaises()) {

                    if (p.getNombre().equals(pais.getNombre())){
                        String urlVideo = p.getUrlVideo();
                        pais.setUrlVideo(urlVideo);
                    }

                     //agregar a la base de datos firestore
                    Map<String,Object> mapa = new HashMap<>();
                    mapa.put("id",p.getId());
                    mapa.put("urlImagen",p.getUrlImagen());
                    mapa.put("nombre",p.getNombre());
                    mapa.put("urlVideo",p.getUrlVideo());
                    mapa.put("poblacion",p.getPoblacion());
                    mapa.put("capital",p.getCapital());

                    db.collection("Pais")
                            .add(mapa)
                            .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                @Override
                                public void onSuccess(DocumentReference documentReference) {
                                    Log.d("FB","Pais agregado correctamente:" + documentReference.getId());
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.w("FB","Pais no agregado",e);
                                }
                            });

                }

                Intent intent = new Intent(getApplicationContext(), VideoPaisActivity.class);
                intent.putExtra("Objeto", (Serializable) pais);
                startActivity(intent);
            }
        });
        activityMainBinding.recyclerPaises.setAdapter(mAdapter);
    }


}
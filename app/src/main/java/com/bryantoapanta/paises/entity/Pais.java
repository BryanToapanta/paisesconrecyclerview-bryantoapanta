package com.bryantoapanta.paises.entity;


import java.io.Serializable;

public class Pais implements Serializable {

    private Integer id;
    private String urlImagen;
    private String nombre;
    private String urlVideo;
    private String poblacion;
    private String capital;

    public Pais(Integer id, String urlImagen, String nombre, String urlVideo, String poblacion, String capital) {
        this.id = id;
        this.urlImagen = urlImagen;
        this.nombre = nombre;
        this.urlVideo = urlVideo;
        this.poblacion = poblacion;
        this.capital = capital;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUrlImagen() {
        return urlImagen;
    }

    public void setUrlImagen(String urlImagen) {
        this.urlImagen = urlImagen;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUrlVideo() {
        return urlVideo;
    }

    public void setUrlVideo(String urlVideo) {
        this.urlVideo = urlVideo;
    }

    public String getPoblacion() {
        return poblacion;
    }

    public void setPoblacion(String poblacion) {
        this.poblacion = poblacion;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }
}

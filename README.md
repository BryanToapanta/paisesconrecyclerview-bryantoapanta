# Deber de Paises con RecyclerView
### Componentes Utilizados
- RecyclerView
- CardView
- Layouts: RelativeLayout, LinearLayout, ConstraintLayout
- Fragment

### Dependencias Utilizadas
    RecyclerView: implementation 'androidx.recyclerview:recyclerview:1.0.0'
    CardView: implementation 'androidx.cardview:cardview:1.0.0'
    YouTubeApi: Usa una libreria Externa descargada de  [https://developers.google.com/youtube/android/player?hl=es]
    Glide: implementation 'com.github.bumptech.glide:glide:4.11.0'

### Capturas de Pantalla
![Lista de paises](https://1.bp.blogspot.com/-n7dQD-YWKGc/XybYYfeTbvI/AAAAAAAAD5I/6-HPqzCjH_gEnJegucpVs6fMR9BskfLdgCLcBGAsYHQ/s1280/WhatsApp%2BImage%2B2020-08-02%2Bat%2B09.48.45.jpeg)

![Lista de paises](https://1.bp.blogspot.com/--uAHYbeIfZU/XybYYVfJIFI/AAAAAAAAD5M/B0un3oTeSTMJeobg0StwJlz4OFXEce4OgCLcBGAsYHQ/s1280/WhatsApp%2BImage%2B2020-08-02%2Bat%2B09.48.45%2B%25281%2529.jpeg)